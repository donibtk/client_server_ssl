# chave, certificado e assinatura:
# openssl genrsa -out key.pem 4096
# openssl req -new -key key.pem -out req.pem
# cp req.pem ../certificadora/newreq.pem
# /etc/ssl/misc/CA.pl -sign
# cp ../certificadora/demoCA/cacert.pem .
# rm ../certificadora/newreq.pem

require 'socket'
require 'openssl'
require 'thread'

listening_port = 9090

# instanciar um servidor TCP
server = TCPServer.new(listening_port)

# instanciar contexto SSL
ssl_context = OpenSSL::SSL::SSLContext.new

# adiciona chave privada e certificado assinado (servidor)
ssl_context.key = OpenSSL::PKey::RSA.new(File.open("key.pem"))
ssl_context.cert = OpenSSL::X509::Certificate.new(File.open("newcert.pem"))

# adiciona certificado publico da certificadora, para validar
# certificado do cliente
cert_store = OpenSSL::X509::Store.new
cert_store.add_file 'cacert.pem'
ssl_context.cert_store = cert_store

# flags para validar chave publica do cliente e interromper a
# conexão caso nenhuma seja fornecida
#flags = OpenSSL::SSL::VERIFY_PEER|OpenSSL::SSL::VERIFY_FAIL_IF_NO_PEER_CERT
#sslContext.verify_mode = flags

# instanciar servidor SSL
ssl_server = OpenSSL::SSL::SSLServer.new(server, ssl_context)

puts "Servidor na porta #{listening_port}"

loop do
  # aceitar novas conexões
  begin
    connection = ssl_server.accept
  rescue
    ssl_server = OpenSSL::SSL::SSLServer.new(server, ssl_context)
    next
  end

  puts connection.session.to_text

  Thread.new {
    begin
      # lê dados do cliente
      while (line_in = connection.gets)
        line_in = line_in.chomp
        $stdout.puts "Cliente: " + line_in
        if line_in == 'agora'
          line_out = "Agora: " + Time.now.to_s
        elsif line_in == 'sair'
          connection.puts 'Desconectando...'
          connection.close
        else
          line_out = 'Não entendi'
        end
        # envia dados para o cliente
        connection.puts line_out
      end
    rescue
      $stdout.puts $!, $@
      next
    end
  }
end
