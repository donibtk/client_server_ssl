local socket = require("socket")
local ssl    = require("ssl")


-- parametros para o contexto ssl
local params = {
   mode = "client",
   -- versão do ssl/tls, obrigatório definir
   protocol = "tlsv1_2",
   -- chave privada do cliente
   key = "key.pem",
   -- Certificado assinado (cliente)
   certificate = "newcert.pem",
   -- certificado publico da certificadora, para verificar
   -- a chave do servidor
   cafile = "cacert.pem",
   -- para verificar o certificado e interromper caso
   -- seja desconhecido ou ausente
   verify = {"peer", "fail_if_no_peer_cert"},
   options = "all",
   --curve = "secp384r1",
}

local host = arg[1]

-- cria um socket TCP
local conn = socket.tcp()

-- conecta ao servidor
conn:connect(host, 9090)

-- cria o contexto SSL
conn = ssl.wrap(conn, params)

-- faz o "aperto de mãos"
-- (não é automático como no ruby)
conn:dohandshake()

local info = conn:info()
for k, v in pairs(info) do
  print(k, v)
end

str = true
while str do
  -- envia dados para o servidor
  -- é necessário terminar com \n (new line)
  conn:send(io.read() .. '\n')
  -- lê retorno do servidor e imprime na tela
  -- (em lua as funções podem retornar mais de um valor,
  -- neste caso três valores são retornados)
  str, err, part = conn:receive("*l")
  if str then print(str) end
end

conn:close()
