require "socket"
require "thread"
require "openssl"

host = ARGV[0]
port = 9090

# instanciar um socket TCP
socket = TCPSocket.new(host, port)

# adiciona chave privada e certificado assinado (cliente)
ctx = OpenSSL::SSL::SSLContext.new
ctx.cert = OpenSSL::X509::Certificate.new(File.open("newcert.pem"))
ctx.key = OpenSSL::PKey::RSA.new(File.open("key.pem"))

# adiciona chave publica da certificadora, para validar certificado
# do servidor
cert_store = OpenSSL::X509::Store.new
cert_store.add_file 'cacert.pem'
ctx.cert_store = cert_store

# flags para validar chave publica do servidor e interromper a
# conexão caso nenhuma seja fornecida
flags = OpenSSL::SSL::VERIFY_PEER|OpenSSL::SSL::VERIFY_FAIL_IF_NO_PEER_CERT
ctx.verify_mode = flags

# instanciar socket SSL (socket tcp e contextoSSL como
# parâmetros)
ssl = OpenSSL::SSL::SSLSocket.new(socket, ctx)

# conectar ao servidor (via socket SSL)
ssl.connect

puts ssl.session.to_text

Thread.new {
  begin
                   # lê dados do servidor
    while lineIn = ssl.gets
      lineIn = lineIn.chomp
      $stdout.puts lineIn
    end
  rescue
    $stderr.puts "Erro: " + $!
  end
}

while (lineOut = $stdin.gets)
  lineOut = lineOut.chomp
  # envia dados para o servidor
  ssl.puts lineOut
end
